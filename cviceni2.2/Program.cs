﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Zadejte prvni cislo: ");
            int cislo1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Zadejte druhe cislo: ");
            int cislo2 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Zadejte treti cislo: ");
            int cislo3 = Convert.ToInt32(Console.ReadLine());

            int tmp;
            if (cislo1 > cislo2)
            {
               if (cislo3 > cislo1)
                {
                    tmp = cislo1;
                    cislo1 = cislo2;
                    cislo2 = tmp;
                }
               else if (cislo3 < cislo1 && cislo2 < cislo3)
                {
                    tmp = cislo1;
                    cislo1 = cislo2;
                    cislo2 = cislo3;
                    cislo3 = tmp;
                }
                else if (cislo1 == cislo3)
                {
                    tmp = cislo1;
                    cislo1 = cislo2;
                    cislo2 = tmp;
                }
                else
                {
                    tmp = cislo1;
                    cislo1 = cislo3;
                    cislo3 = tmp;
                }
            }
            else
            {
                if (cislo1 > cislo3)
                {
                    tmp = cislo1;
                    cislo1 = cislo3;
                    cislo3 = cislo2;
                    cislo2 = tmp;
                }
                else if (cislo1 < cislo3 && cislo3 < cislo2)
                {
                    tmp = cislo3;
                    cislo3 = cislo2;
                    cislo2 = tmp;
                }
                else if (cislo1 == cislo3)
                {
                    tmp = cislo2;
                    cislo2 = cislo3;
                    cislo3 = tmp;
                }
            }


            Console.WriteLine("Serazena cisla: {0}, {1}, {2}", cislo1, cislo2, cislo3);
            Console.ReadLine();
        }
    }
}
