﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni2._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Spocteme nejmensi spolecny nasobek cisel");
            Console.Write("Zadejte prvni cislo: ");
            int cislo1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Zadejte druhe cislo: ");
            int cislo2 = Convert.ToInt32(Console.ReadLine());

            int zbytek;
            int nsn = cislo1 * cislo2;

            while ( cislo2 != 0 ) // Eukliduv algoritmus
            {
                zbytek = cislo1 % cislo2;
                cislo1 = cislo2;
                cislo2 = zbytek;
            }

            nsn = nsn / cislo1;

            Console.WriteLine("Nejmensi spolecny nasobek je {0}.", nsn);
            Console.ReadLine();
            
        }
    }
}
