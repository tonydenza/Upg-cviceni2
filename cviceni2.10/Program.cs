﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni2._10
{
    class Program
    {
        static void Main(string[] args)
        {
            int pocet = 0, stare = 0; ;
            bool konst = true, kles = true, rost = true, nekles = true, nerost = true;
            Console.WriteLine("Zjistime druh posloupnosti.");
            while (true)
            {
                Console.Write("Zadejte cislo: ");
                int cislo = Convert.ToInt32(Console.ReadLine());
                if (cislo <= 0)
                    break;

                pocet++;

                if (stare != cislo && stare != 0)
                    konst = false;

                if (stare <= cislo && stare != 0)
                    kles = false;

                if (stare >= cislo)
                    rost = false;

                if (stare < cislo && stare != 0)
                    nerost = false;

                if (stare > cislo)
                    nekles = false;

                stare = cislo;

            }

            if (pocet == 0)
            {
                Console.WriteLine("Nezadal jste zadne cislo.");
            }
            else if (pocet == 1)
            {
                Console.WriteLine("Posloupnost je kratka.");
            }
            else if (konst)
            {
                Console.WriteLine("Posloupnost je konstantni.");
            }
            else if (kles)
            {
                Console.WriteLine("Posloupnost je klesajici.");
            }
            else if (rost)
            {
                Console.WriteLine("Posloupnost je rostouci.");
            }
            else if (nekles)
            {
                Console.WriteLine("Posloupnost je neklesajici.");
            }
            else if (nerost)
            {
                Console.WriteLine("Posloupnost je nerostouci.");
            }
            else
            {
                Console.WriteLine("Posloupnost neni monotonni.");
            }

            Console.ReadLine();
        }
    }
}
