﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni2._7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Vsechna dokonala cisla do 10000 jsou: ");
            int cislo = 1;
            int i = 1;
            int dok = 0;

            while (cislo < 10000)
            {
                while (i < cislo)
                {
                    if (cislo % i == 0)
                    {
                        dok = dok + i;
                        i++;
                    }
                    else
                    {
                        i++;
                    }
                }

                if (dok == cislo)
                {
                    Console.Write("{0} ", cislo);
                }
                cislo++;
                i = 1;
                dok = 0;
            }
            Console.ReadLine();
        }
    }
}
