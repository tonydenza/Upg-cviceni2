﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni2._8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Vsechna armstrongova cisla do 100000 jsou: ");
            int cislo = 1;
            int puvod, exp = 0;
            int arm = 1, zbytek, vysledek = 0;

            while (cislo < 100000)
            {
                puvod = cislo;
                while (puvod != 0)
                {
                    puvod = puvod / 10;
                    exp++;
                }

                puvod = cislo;
                while (puvod != 0)
                {
                    zbytek = puvod % 10;
                    for (int i=1; i<=exp; i++)
                    {
                        arm *= zbytek;
                    }
                    vysledek += arm;
                    arm = 1;
                    puvod = puvod / 10;
                }

                if (vysledek == cislo)
                {
                    Console.Write("{0} ", cislo);
                }

                cislo++;
                vysledek = 0;
                exp = 0;
            }
            Console.ReadLine();
        }
    }
}
