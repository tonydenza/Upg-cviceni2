﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni2._6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Vsechna prvocisla do 1000 jsou: ");
            int cislo = 1;
            int i = 1;
            bool prv = true;

            while (cislo < 1000)
            {
                while (i < cislo)
                {
                    if (cislo % i == 0 && i != 1)
                    {
                        prv = false;
                        break;
                    }

                    else if (prv && i+1==cislo)
                    {
                        Console.Write("{0} ", cislo);
                        i++;
                        break;
                    }
                    i++;
                }

                cislo++;
                i = 1;
                prv = true;
            }


            Console.ReadLine();
        }
    }
}
