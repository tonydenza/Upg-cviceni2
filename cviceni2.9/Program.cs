﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni2._9
{
    class Program
    {
        static void Main(string[] args)
        {
            int pocet = 0, soucet = 0, min = 0, max = 0, min2 = 0, max2 = 0;
            while (true)
            {
                Console.Write("Zadejte cislo: ");
                int cislo = Convert.ToInt32(Console.ReadLine());
                if (cislo <= 0)
                    break;
                pocet++;
                soucet += cislo;

                if (cislo <= max && cislo > max2)
                    max2 = cislo;

                if (cislo > max)
                {
                    max2 = max;
                    max = cislo;
                }

                if (pocet == 1)
                {
                    min = cislo;
                }

                if (pocet == 2)
                {
                    min2 = cislo;
                }

                if (cislo >= min && cislo < min2)
                    min2 = cislo;

                if (cislo < min)
                {
                    min2 = min;
                    min = cislo;
                }
            }
     
            if (pocet==1)
            {
                Console.WriteLine("Pocet cisel: {0}.", pocet);
            }
            else if (pocet == 0)
            {
                Console.WriteLine("Nezadal jste zadne cislo.");
            }
            else
            {
                Console.WriteLine("Pocet cisel: {0}, soucet cisel: {1} ", pocet, soucet);
                Console.WriteLine("Nejvyssi cislo: {0}, druhe nejvyssi: {1} ", max, max2);
                Console.WriteLine("Nejnizsi cislo: {0}, druhe nejnizsi cislo: {1} ", min, min2);
            }

            Console.ReadLine();
        }
    }
}
