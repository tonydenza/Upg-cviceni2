﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni2._4
{
    class Program
    {
        static void Main(string[] args)
        {
            int i=1, fakt=1;

            Console.Write("Zadejte cislo: ");
            int n = Convert.ToInt32(Console.ReadLine());

            if (n < 0)
            {
                Console.WriteLine("Nezadal jsi kladne cislo.");
                Console.ReadLine();
            }
            else if (n==0)
            {
                fakt = 1;
            }
            else
            {
                while (i<=n)
                {
                    fakt *= i;
                    i++;
                }
                Console.WriteLine("Faktorial zadaneho cisla je {0}", fakt);
                Console.ReadLine();
            }

        }
    }
}
