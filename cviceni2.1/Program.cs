﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni2._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Zadejte prvni cislo: ");
            int cislo1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Zadejte druhe cislo: ");
            int cislo2 = Convert.ToInt32(Console.ReadLine());

            if (cislo1 > cislo2)
            {
                Console.Write("Serazena cisla: {0}, {1}", cislo1, cislo2);
                Console.ReadLine();
            }
            else
            {
                int tmp = cislo1;
                cislo1 = cislo2;
                cislo2 = tmp;
                Console.Write("Serazena cisla: {0}, {1}", cislo1, cislo2);
                Console.ReadLine();
            }
        }
    }
}
