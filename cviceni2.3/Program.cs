﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni2._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Vsechna dvojciferna cisla jsou: ");
            int i;
            for (i=0; i < 100; i++)
            {
                if (i < 10)
                    continue;
                else
                    Console.Write("{0} ", i);
            }
            Console.ReadLine();
        }
    }
}
